
# Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))

# choco feature enable -n allowGlobalConfirmation

# choco install git -params '"/GitAndUnixToolsOnPath"'

Set-Location -Path C:\

New-Item -ItemType Directory -Force -Path C:\Websites\Development

cd C:\Websites\Development

git clone https://gitlab.com/pedrocondor/datetime.git

Invoke-WebRequest "https://master.dockerproject.org/windows/x86_64/docker.zip" -OutFile "docker.zip" -UseBasicParsing

Expand-Archive -Path docker.zip 
   
$Path = [Environment]::GetEnvironmentVariable("Path",[System.EnvironmentVariableTarget]::Machine)
[Environment]::SetEnvironmentVariable("Path", $Path + ";C:\Docker", [EnvironmentVariableTarget]::Machine)

exit
