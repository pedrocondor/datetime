# Dockerized NetCore App 

## Install Git (if not installed) 
### Instructions to install Git (Execute this commands using powershell with elevated privileges) : 

 1. set-executionpolicy -executionpolicy unrestricted
 2. Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
 3. choco feature enable -n allowGlobalConfirmation
 4. choco install git -params '"/GitAndUnixToolsOnPath"'
 5. exit
 
## Steps to Deploy:
### (Execute this commands using powershell with elevated privileges)

 1. Clone this repository to your temp enviroment.
 2. Execute install.ps1 script using powershell with elevated privileges.
 3. Execute deploy.ps1 script using powershell with elevated privileges.
 4. Navigate to http://localhost on your browser.
